<!--

var timerId;
var delay=1000;
var dst_str;

//--------------------------------------------------------------------
function DisplayClock(maxnum, showsec, showdst, param)
{
	// In: maxnum   1 to 8 (number of locations to display)
	//     showsec  0 or 1
	//     showdst  0 or 1
	//     param 	array - param[1], param[2], param[3], param[4] ....
	var loc, diff, dst, i;

	for(i = 1 ; i <= maxnum ; i++) {
		loc = param[i];
		// convert string parameters to integer
		for(j = 0; j <= 11; j++) {
			switch(j) {
				case 1:			// loc_name (= string)
					continue;
				case 2: 		// loc_diff (= float)
					loc[j] = parseFloat(loc[j]); 
					break;
				default:		// others (= integer)
					loc[j] = parseInt(loc[j]);
					break;
			}
		}
		// ---- loc array ----
		// [0] 	loc_no
		// [1] 	loc_name
		// [2] 	loc_diff 
		// [3] 	loc_dst
		// [4]	loc_smon
		// [5]	loc_sth
		// [6]	loc_sdow
		// [7]	loc_stime
		// [8]	loc_emon
		// [9]	loc_eth
		// [10]	loc_edow
		// [11]	loc_etime
		if(loc[0] == 0) continue;
		diff = loc[2];
		dst  = loc[3];
		if(showdst) {
			if(dst == 1) {
				if(IsDST(loc)) {
					diff++;
					dst_str = " *";
				}
				else {
					dst_str = "  ";
				}
			}
			else {
				dst_str = "  ";
			}
		}
		else {
			dst_str = "";
		}
		idname = "worldclock" + i;
		document.getElementById(idname).innerHTML 
						= GenerateTimeString(diff, showsec) + dst_str;
	}
	timerId = setTimeout("DisplayClock(maxnum, showsec, showdst, param)", delay);
}

//--------------------------------------------------------------------
function IsDST(loc)
{
	// NOTE: this function is called only for the countries 
	//       which has the summer time (DST)
	// ---- loc array ----
	// [0] 	loc_no
	// [1] 	loc_name
	// [2] 	loc_diff 
	// [3] 	loc_dst
	// [4]	loc_smon
	// [5]	loc_sth
	// [6]	loc_sdow
	// [7]	loc_stime
	// [8]	loc_emon
	// [9]	loc_eth
	// [10]	loc_edow
	// [11]	loc_etime
	var localDate, localTime, localYear, startYear, endYear;
	var tzDiff, tzOffset, dstStartGMT, dstEndGMT, nowGMT;

	// calculate time difference from GMT (in minute)
	tzDiff = loc[2] * 60; 

	localDate = new Date();
	localTime = localDate.getTime();	// in msec
	localYear = localDate.getFullYear();
	startYear = endYear = localYear;
	if(loc[4] > loc[8]) {
		// e.g DST starts Nov 1 and ends March 30 of the next year
		//     This happens with the countries in southern half
		//	   such as Australia, New Zealand
		endYear++;
	}

	// time difference between GMT and local time in minute
	tzOffset = localDate.getTimezoneOffset();

	dstStartGMT = GetDstTime(startYear, loc[4], loc[5], loc[6], loc[7]);
	dstEndGMT = GetDstTime(endYear, loc[8], loc[9], loc[10], loc[11]);

	nowGMT = localTime + (tzOffset + tzDiff) * 60 * 1000;

	if((dstStartGMT <= nowGMT) && (nowGMT < dstEndGMT)) {
		return 1;
	}
	return 0;
}

//--------------------------------------------------------------------
function GenerateTimeString(nDiff, bShowSec)
{
	var dtNowLocal = new Date;
	var dtNow = new Date;
	var strHour, strMin, strSec, strAmPm, strColon;
	var strMon, strDate;
	var strDay = new Array("SUN", "MON", "TUE", "WED", "THR", "FRI", "SAT");

    var today = new Date();
    var year = today.getFullYear();

	dtNow.setTime(dtNowLocal.getTime() + (dtNowLocal.getTimezoneOffset() + (nDiff * 60)) * 60 * 1000);

	strMon  = dtNow.getMonth() + 1;
	strDate = dtNow.getDate();
	if(strMon  < 10) strMon  = " " + strMon;
	if(strDate < 10) strDate = " " + strDate;

	strHour = dtNow.getHours();
	strMin  = dtNow.getMinutes();
	strSec  = dtNow.getSeconds();
	strAmPm = ((strHour < 12)? " AM": " PM");
	if(strHour >= 12) strHour -= 12;
	if(strHour < 10) strHour = "0" + strHour;
	if(strMin  < 10) strMin  = "0" + strMin;
	if(strSec  < 10) strSec  = "0" + strSec;
	strColon = ":";
	//-- flashing colon
	// strColon = ((strSec % 2)? ":" : " ");

	if(bShowSec) {
		return(strMon+"/"+strDate+"&nbsp;&nbsp;"+strHour+strColon+strMin+strColon+strSec+strAmPm);
	}
	else {
		return(strMon+"/"+strDate+"&nbsp;&nbsp;"+strHour+strColon+strMin+strAmPm);
	}	
}

//--------------------------------------------------------------------
function GetDstTime(year, mon, th, dow, tim) 
{
	var dst_day;
	var hit_cnt;
	var i;
	var numDays = new Array(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

	if(th == 9) {	// last
		dst_day = new Date(year, mon-1, numDays[mon], tim, 0, 0); 
		// decrement date starting from the last day of the month
		for (i = numDays[mon]; i >= 1; i--) {
			dst_day.setDate(i);
			if (dst_day.getDay() == dow) {
				break;	// hit!
			}
		}
	}
	else {	// 1st, 2nd, 3rd, 4th..
		dst_day = new Date(year, mon-1, 1, tim, 0, 0); 
		// increment date starting from the first day of the month
		for (i = 1, hit_cnt = 0; i <= 31; i++) {
			dst_day.setDate(i);
			if (dst_day.getDay() == dow) {
				hit_cnt++;	// hit!
				if(hit_cnt == th) {
					break;
				}
			}
		}
	}
	return dst_day.getTime();
}

// -->
